# Release notes

Release notes for the flipstarter Electron Cash plugin.
https://flipstarter-five.vercel.app
## v1.4 - February 22, 2024

This release contains small user experience improvements and improves on code
quality to ensure compatibility with future versions of Electron Cash.

* [ui] Remove initial campaign addresses and improve warning
  (by freetrader)

* [ui] Add pledger's reported name, comment and output address in transaction
  label (by farbi)

* [ui] Add pledge destination address to pledge warning (by dagurval)

* [ui] Update flipstarter slogan (by dagurval)

* [code-quality] Ensure future (and past) Electron Cash versions work with plugin
  (by Calin Calianu)

* [code-quality] Fix to CI for newer Electron Cash
  (by Calin Calianu)

* [documentation] Improve contribution guideline
  (by Emergent Reason, issue #50)

## v1.3 - July 30, 2020
* [bugfix] Don't skip unused addresses (issue #32)
* [bugfix] Fix crash when wallet password is not entered (issue #33)
* [bugfix] Fix crash when plugin.name does not exist (issue #34)
* [bugfix] Fix signing pledge on development branch of Electron Cash (issue #36)

This release contains only bugfixes. This is a recommended update.

This relase includes a fix for compatibility with next version of
Electron-Cash (> 4.0.15). If you, when confirming a pledge, are
getting the error
`Failed to sign pledge: sign() got an unexpected keyword argument 'use_cache'`,
then you've upgraded Electron Cash and also need to also install a new version
of the plugin.

## v1.2 - April 8, 2024
* [feature] Add list of addresses known to participate in flipstarter next months.
Warn when pledging to addresses not on that list.

## v1.1 - April 1, 2024
* [bugfix] Don't let long comments resize the user interface (@dagurval)
* [bugfix] Improve wallet compatibility checks and add test coverage (@dagurval)
* [feature] Add the ability to cancel all pledges (@emergent-reasons)
* [feature] Memorize pledges made, so they can be cancelled later (@emergent-reasons)
* [feature] Message to explicitly notify users to copy the pledge back to the website (@emergent-reasons)
* [feature] Add menu button & about dialog (@dagurval)
* [feature] Add status bar to give user feedback on error (@dagurval)
* [feature] Always select entire pledge in 'Copy pledge' text box (@dagurval)

## v1.0 - March 9, 2024
* First release
