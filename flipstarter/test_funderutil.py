from decimal import Decimal
from unittest import mock
import unittest

from electroncash.storage import WalletStorage

from .funderutil import _forget_pledge
from .funderutil import _parse_strict_int
from .funderutil import cancel_pledge
from .funderutil import coin_to_outpoint
from .funderutil import decode_template
from .funderutil import FLIPSTARTER_MEMORIZED_PLEDGES_KEY
from .funderutil import get_pledge_output_summary
from .funderutil import get_pledges
from .funderutil import memorize_pledge
from .funderutil import sats_to_bch
from .funderutil import serialize_pledge_input
from .funderutil import template_to_tx
from .funderutil import sanitize_data

from .test_walletutil import _basic_coin


class TestFunderUtils(unittest.TestCase):

    def test_decode_template(self):
        payload = 'ewogICJvdXRwdXRzIjogW3sKICAgICJ2YWx1ZSI6IDEwMDAwMCwKICAgICJhZGRyZXNzIjogImJpdGNvaW5jYXNoOnBwOHNrdWRxM3g1aHp3OGV3N3Z6c3c4dG40azh3eHNxc3YwbHQwbWYzZyIKCiAgfSwgewogICAgInZhbHVlIjogMTAwMDAwLAogICAgImFkZHJlc3MiOiAiYml0Y29pbmNhc2g6cXJzcnZ0Yzk1Z2c4cnJhZzdkZ2UzamxuZnM0ajlwZTB1Z3JtZW1sOTUwIgoKICB9XSwKICAiZGF0YSI6IHsKICAgICJhbGlhcyI6ICJLaW5kIHN0cmFuZ2VyIiwKICAgICJjb21tZW50IjogIlNhdmUgdGhlIHdoYWxlcyEiCiAgfSwKICAiZG9uYXRpb24iOiB7CiAgICAiYW1vdW50IjogMTA2ODQ0CiAgfSwKICAiZXhwaXJlcyI6IDE1ODEzNzU1OTkKCn0='

        expected = {
                "outputs": [
                    {
                        "value": 100000,
                        "address": bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3
"
                    }, {
                        "value": 100000,
                        "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3
"
                    }
                ],
                "data": {
                    "alias": "Kind stranger",
                    "comment": "Save the whales!"
                },
                "donation": {
                    "amount": 106844
                },
                "expires": 1581375599
        }

        res = decode_template(payload, check_expired = False)
        self.assertDictEqual(expected, res)

    def test_default_sanitize_data(self):
        expected = 'default'
        result = sanitize_data('', 'default')
        self.assertEqual(expected, result)

    def test_sanitize_data(self):
        expected = 'an &lt;script&gt;evil()&lt;/script&gt; example'
        res = sanitize_data('an <script>evil()</script> example', 'default')
        self.assertEqual(expected, res)

    def test_get_pledge_output_summary(self):
        data = {
            "outputs": [
                {
                    "value": 100,
                    "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3"
                }, {
                    "value": 300,
                    "address": "pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0lt0beta"
                }, {
                    "value": 400,
                    "address": "pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0ltgamma"
                }, {
                    "value": 200,
                    "address": "pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0ltdelta"
                }
            ]}

        expected = 'pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0ltgamma + 3 others'

        res = get_pledge_output_summary(data['outputs'])
        self.assertEqual(expected, res)

    def test_get_one_pledge_output_summary(self):
        data = {
            "outputs": [
                {
                    "value": 100,
                    "address": "pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0ltalpha"
                }
            ]}

        expected = 'bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3'

        res = get_pledge_output_summary(data['outputs'])
        self.assertEqual(expected, res)

    def test_sats_to_bch(self):
        self.assertEqual(Decimal('100000.66666668'), sats_to_bch(10000066666668))

    def test_template_to_tx(self):
        tpl = {
                "outputs": [
                    {
                       "value": 1012300000,
                       "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3",
                    }, {
                        "value": 100000,
                        "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3"
                    }
                ],
                "donation": {
                    "amount": 3333,  # unused in test
                },
                "expires": 1580507999
        }

        dummy_input = {
            'address': "bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a",
            'type': 'p2pkh',
            'prevout_hash': '7a0e3fcbdaa9ecc6ccce1ad325b6b661e774a57f2e8519c679964e2dd32e200f',
            'prevout_n': 0,
            'value': 1000,
        }

        tx = template_to_tx(tpl, dummy_input)
        self.assertEqual(1, len(tx.inputs()))
        self.assertDictEqual(dummy_input, tx.inputs()[0])
        self.assertEqual(0, tx.locktime)
        self.assertEqual(2, tx.version)
        self.assertEqual(2, len(tx.get_outputs()))

        self.assertEqual(
                'pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0lt0mf3g',
                str(tx.get_outputs()[0][0]))
        self.assertEqual(
                1012300000,
                tx.get_outputs()[0][1])

        self.assertEqual(
                'qrsrvtc95gg8rrag7dge3jlnfs4j9pe0ugrmeml950',
                str(tx.get_outputs()[1][0]))
        self.assertEqual(
            100000,
            tx.get_outputs()[1][1])

    def test_serialize_pledge_input(self):
        tpl = {
                "outputs": [
                    {
                        "value": 1012300000,
                        "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3,
                    }, {
                        "value": 100000,
                        "address": "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3"
                    }
                ],
                "donation": {
                    "amount": 3333,  # unused in test
                },
                "expires": 1580507999
        }

        dummy_input = {
            'address': "bitcoincash:qrfmvutyysfre5wxvv0wvhkxhrc8qwdkxuseg99xh3",
            'type': 'p2pkh',
            'prevout_hash': '7a0e3fcbdaa9ecc6ccce1ad325b6b661e774a57f2e8519c679964e2dd32e200f',
            'prevout_n': 0,
            'value': 1000,
            'sequence': 0xffffffff,
            'x_pubkeys': [],
            'pubkeys': []
        }
        tx = template_to_tx(tpl, dummy_input)

        serialized = serialize_pledge_input(tx, "anonymous", "save the whales",
                                            allow_empty_signatures=True)

        import base64
        import json
        p = json.loads(base64.b64decode(serialized, validate=True))

        self.assertTrue("inputs" in p)
        self.assertTrue(isinstance(p["inputs"], list))

        # The plugin supports only one input
        self.assertEqual(1, len(p["inputs"]))

        for i in p["inputs"]:
            self.assertTrue("previous_output_transaction_hash" in i)
            self.assertTrue("previous_output_index" in i)
            self.assertTrue("unlocking_script" in i)
            self.assertTrue("sequence_number" in i)

        self.assertTrue("data" in p)
        self.assertTrue("alias" in p["data"])
        self.assertTrue("comment" in p["data"])

        self.assertTrue("data_signature" in p)

        # We don't support signing data yet.
        self.assertEqual(None, p["data_signature"])

    def test_parse_strict_int(self):
        # numerical inputs
        self.assertEqual(5, _parse_strict_int(5))
        self.assertRaises(ValueError, _parse_strict_int, 5.5)  # floats with decimals

        # string inputs
        self.assertEqual(5, _parse_strict_int("5"))
        self.assertRaises(ValueError, _parse_strict_int, "5.5")  # string floats with decimals

        # other inputs
        class InvalidType:
            pass
        self.assertRaises(TypeError, _parse_strict_int, InvalidType)


class TestCancellation(unittest.TestCase):
    def setUp(self):
        self.coin_1 = _basic_coin("1111", 1)
        self.coin_1_outpoint = "1111:1"
        self.coin_2 = _basic_coin("2222", 2)
        self.coin_2_outpoint = "2222:2"

    def test_memorize_pledge_stores_pledge_outpoint_to_wallet(self):
        storage = WalletStorage("", in_memory_only=True)

        # confirm before / after
        self.assertIsNone(storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY))
        memorize_pledge(storage, self.coin_1)
        self.assertIn(self.coin_1_outpoint, storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY))

    def test_get_pledges_retrieves_intersection_of_memorized_and_actually_existing_coins(self):
        test_cases = [
            {
                "name":      "0 pledges memorized; no utxos",
                "memorized": {},
                "utxos":     [],
                "expected":  [],
            }, {
                "name": "1 pledges memorized; no utxos",
                "memorized": {self.coin_1_outpoint: None},
                "utxos":     [],
                "expected":  [],
            }, {
                "name":      "1 pledges memorized; matching utxos",
                "memorized": {self.coin_1_outpoint: None},
                "utxos":     [self.coin_1],
                "expected":  [self.coin_1],
            }, {
                "name":      "1 pledges memorized; matching + extra utxos",
                "memorized": {self.coin_1_outpoint: None},
                "utxos":     [self.coin_1, self.coin_2],
                "expected":  [self.coin_1],
            }, {
                "name":      "2 pledges memorized; only 1 matching utxo",
                "memorized": {self.coin_1_outpoint: None, self.coin_2_outpoint: None},
                "utxos":     [self.coin_1, self.coin_2],
                "expected":  [self.coin_1, self.coin_2],
            }
        ]
        for tc in test_cases:
            storage = WalletStorage("", in_memory_only=True)
            storage.put(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, tc["memorized"])
            actual_pledges = get_pledges(storage, tc["utxos"])
            self.assertCountEqual(actual_pledges, tc["expected"], tc["name"])

    @mock.patch("flipstarter.funderutil.spend_coin_to_self")
    def test_cancel_pledge_spends_a_pledge_coin_and_forgets_it(self, m_spend_coin_to_self):
        # prepare a memorized coin
        m_wallet = mock.MagicMock()
        m_wallet.storage = WalletStorage("", in_memory_only=True)
        m_wallet.storage.put(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, {self.coin_1_outpoint: None})

        def confirm_is_memorized(is_memorized):
            args = [self.coin_1_outpoint, m_wallet.storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY)]
            if is_memorized:
                self.assertIn(*args)
            else:
                self.assertNotIn(*args)

        # confirm memorized before cancel
        confirm_is_memorized(True)

        # do the cancellation
        cancel_pledge(m_wallet, "password", mock.MagicMock(), self.coin_1)

        # confirm that we spent the coin
        m_spend_coin_to_self.assert_called_once()

        # confirm that we forgot the coin
        confirm_is_memorized(False)

    def test__forget_pledge_removes_it_from_wallets_flipstarter_memory(self):
        # no error if doesn't exist
        storage = WalletStorage("", in_memory_only=True)
        _forget_pledge(storage, self.coin_1)

        # setup memory ...
        storage = WalletStorage("", in_memory_only=True)
        memorized = {self.coin_1_outpoint: None}
        storage.put(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, memorized)
        self.assertEqual(storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY), memorized)
        # ... and confirm removal
        _forget_pledge(storage, self.coin_1)
        self.assertEqual(storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY), {})

    def test_coin_to_outpoint(self):
        coin = _basic_coin('4be50bef3d8009d17e41441b643f85ef3f1c3f97c4adfe5d49eb8e38f0f844ca', 1)
        expected = '1b49309fbc74916e0dcb87e1f8d060381a8f7b8ea08afe9e74babf4987fe9777:1'
        actual = coin_to_outpoint(coin)
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
