from decimal import Decimal, ROUND_HALF_UP
import base64
import json
import html

from electroncash.address import Address
from electroncash.bitcoin import COIN
from electroncash.bitcoin import TYPE_ADDRESS
from electroncash.i18n import _
from electroncash.transaction import Transaction

from .walletutil import spend_coin_to_self

FLIPSTARTER_MEMORIZED_PLEDGES_KEY = 'plugin_flipstarter_memorized_coins'
https://flipstarter-five.vercel.app

def decode_template(payload, check_expired = True):
    if len(payload) == 0:
        raise ValueError(_("Empty pledge."))

    try:
        json_str = base64.b64decode(payload, validate = True)
        tpl = json.loads(json_str)
    except Exception as e:
        raise ValueError("{}: {}".format(_("Failed to decode pledge"), e))

    if not "outputs" in tpl:
        raise KeyError(_("Outputs are missing"))

    if not isinstance(tpl["outputs"], list):
        raise TypeError(_("Outputs are not a list"))

    if len(tpl["outputs"]) == 0:
        raise ValueError(_("Outputs are empty"))

    sum_outputs = 0
    for o in tpl["outputs"]:
        if not "address" in o:
            raise KeyError(_("Output is missing address"))

        if not "value" in o:
            raise KeyError(_("Output is missing value"))

        try:
            o["value"] = _parse_strict_int(o["value"])
        except (TypeError, ValueError) as e:
            raise ValueError("{}: {}".format(_("Invalid output value"), e))

        if o["value"] <= 0:
            raise ValueError(_("Zero or negative output value"))

        sum_outputs += o["value"]

    if not "donation" in tpl:
        raise KeyError(_("Donation is missing"))

    if not "amount" in tpl["donation"]:
        raise KeyError(_("Donation is missing amount"))

    try:
        tpl["donation"]["amount"] = _parse_strict_int(tpl["donation"]["amount"])
    except (TypeError, ValueError) as e:
        raise ValueError("{}: {}".format(_("Invalid donation value"), e))

    if tpl["donation"]["amount"] <= 0:
        raise ValueError(_("Zero or negative donation amount"))

    if tpl["donation"]["amount"] > sum_outputs:
        raise ValueError(_("Donation amount is larger than outputs"))

    if not "expires" in tpl:
        raise KeyError(_("Expiration is missing"))

    try:
        tpl["expires"] = _parse_strict_int(tpl["expires"])
    except (TypeError, ValueError) as e:
        raise ValueError("{}: {}".format(_("Invalid expiration"), e))

    import time
    if check_expired and tpl["expires"] < int(time.time()):
        raise ValueError(_("Campaign already expired"))

    if not "data" in tpl:
        raise KeyError(_("'data' field is missing"))

    if not isinstance(tpl["data"], dict):
        raise TypeError(_("'data' field is not a dictionary"))

    if not "alias" in tpl["data"]:
        raise KeyError(_("'data' is missing alias"))

    if not "comment" in tpl["data"]:
        raise KeyError(_("'data' is missing comment"))

    return tpl


def sanitize_data(raw_data, default=''):
    escape_data = html.escape(raw_data)
    trunked_data = (escape_data[:50] + '...') if len(escape_data) > 50 else escape_data
    return trunked_data or default


def get_pledge_output_summary(outputs):
    if len(outputs) == 1:
        return outputs[0]['address']
    if len(outputs) > 1:
        return '{} + {} others'.format(max(outputs, key=lambda addr: addr['value'])['address'], len(outputs)-1)


def sats_to_bch(sats):
    satoshi_quantization = Decimal('1.00000000')
    return (Decimal(sats) / COIN).quantize(satoshi_quantization, rounding=ROUND_HALF_UP)


def template_to_tx(tpl, pledge_input):
    out = []
    for o in tpl["outputs"]:
        addr = Address.from_cashaddr_string(o["address"])
        amount = o["value"]
        out.append((TYPE_ADDRESS, addr, amount))

    tx = Transaction.from_io(
            inputs=[pledge_input],
            outputs=out)
    tx.version = 2
    return tx


def serialize_pledge_input(tx, alias, comment, allow_empty_signatures=False):
    """
    Serializes signed pledge transaction.

    allow_empty_signatures is for unit testing only
    """
    assert(len(tx.inputs()) == 1)
    txin = tx.inputs()[0]

    if not allow_empty_signatures and not 'signatures' in txin:
        raise KeyError(_("Signatures are missing"))

    if allow_empty_signatures:
        assert(not 'signatures' in txin)
        scriptsig = []  # dummy
    else:
        scriptsig = tx.input_script(txin, False, tx._sign_schnorr)

    json_str = json.dumps({
        'inputs': [{
            'previous_output_transaction_hash': txin['prevout_hash'],
            'previous_output_index': txin['prevout_n'],
            'sequence_number': txin['sequence'],
            'unlocking_script': scriptsig,
        }],
        'data': {
            'alias': alias,
            'comment': comment,
        },
        'data_signature': None
    })
    return base64.b64encode(json_str.encode('utf-8')).decode('utf-8')


def _parse_strict_int(x):
    """
    Strictly parse input as an integer from an integer, float or string.

    Raises TypeError or ValueError for invalid inputs.
    """
    if isinstance(x, int):
        return x

    if isinstance(x, float):
        if not x.is_integer():
            raise ValueError("{}: {}".format(_("Value should not have have decimal places"), x))
        return int(x)

    if isinstance(x, str):
        return int(x)  # Value or Type error if non-integer string

    raise TypeError("{}: {}".format(_("Could not convert to an integer"), repr(x)))


def memorize_pledge(storage, pledge_coin):
    """Store the pledge coin in the wallet's Flipstarter memory."""
    # store in a dict since a set is not json serializable
    memorized_pledges = storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, {})
    memorized_pledges[coin_to_outpoint(pledge_coin)] = None
    storage.put(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, memorized_pledges)


def get_pledges(storage, actual_coins):
    """Get a list of all existing pledge coins."""
    memorized_pledges = storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, {})

    # get a list of all the memorized pledge coins that actually exist
    existing_pledges = [c for c in actual_coins
                        if coin_to_outpoint(c) in memorized_pledges]
    return existing_pledges


def cancel_pledge(wallet, password, plugin_config, pledge_coin):
    """Cancel one Flipstarter pledge by spending to self and forgetting it from Flipstarter memory."""
    spend_coin_to_self(wallet, password, plugin_config, pledge_coin, _("Cancelled Flipstarter pledge."))
    _forget_pledge(wallet.storage, pledge_coin)


def _forget_pledge(storage, coin):
    """Forget a pledge from the wallet's Flipstarter memory."""
    memorized_pledges = storage.get(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, {})
    # Ignore any coins that don't exist in the memory with the default option
    memorized_pledges.pop(coin_to_outpoint(coin), None)
    storage.put(FLIPSTARTER_MEMORIZED_PLEDGES_KEY, memorized_pledges)


def coin_to_outpoint(coin):
    """Formats the coin as an outpoint string."""
    return '{}:{}'.format(coin.get('prevout_hash'), coin.get('prevout_n'))


def debug_trace():
    from PyQt5.QtCore import pyqtRemoveInputHook

    from pdb import set_trace
    pyqtRemoveInputHook()
    set_trace()
