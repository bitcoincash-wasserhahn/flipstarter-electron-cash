# Flipstarter Electron Cash Plugin

A plugin for Electron Cash that allows you to join crowdsourced funding of projects.

This plugin is licensed under the MIT open source license.

*EXPERIMENTAL*: Use at own risk.

[Release notes](RELEASE-NOTES.md)


## Download

See [Releases](https://gitlab.com/flipstarter/flipstarter-electron-cash/-/releases)


## Contribute

The Flipstarter team is a group of volunteers, and we welcome contributions.

General guidelines:

- Submit merge requests for the campaign backend and frontend [here](https://gitlab.com/flipstarter/backend/-/merge_requests).
- Submit merge requests for this Electron Cash plugin [here](https://gitlab.com/flipstarter/flipstarter-electron-cash/-/merge_requests).
- Keep each change minimal and self-contained. Note that "minimal" does not always mean small, but usually it does.
- Avoid mixing moving stuff around with changing stuff. Do changes with renames on their own.
- Code review is an important part of the development process. Contributors and maintainers should expect constructive communication from each other.
- In some cases, merge requests are rejected. It is up to the MR author to convince the reviewers that the changes warrant the review effort.

Suggested workflow:

1. Find or create an issue describing a need or problem that needs to be solved.
1. [Optional] If you are interested in a bounty, please follow this guideline. Please note that although maintainers try to be realistic about bounty expectations, sometimes MRs are not accepted, and final payout of a bounty depends only on approval and merge of changes.
    1. Create a thread on the issue to discuss your proposal and bounty.
    1. Propose a problem to be solved (any clarification to the issue), the outline of a solution and a bounty.
        - Bounties are only paid in Bitcoin Cash.
        - Bounties are typically denominated in USD.
        - BCH conversion rates are determined at the time of transfer, typically by the Electron Cash wallet's price source.
    1. Review and revise the proposal with the maintainers.
    1. Flipstarter team pays the bounty from public donations only after a successful review, revision and merge process.
1. Create a Merge/Pull Request with the proposed changes.
1. Work with maintainers to review and revise the proposed changes.
1. Maintainers merge changes if/when they meet Flipstarter needs.


## Development

### Testing in Electron Cash

Symlink the `flipstarter` folder of this repo into the folder
`Electron-Cash/electroncash_plugins`. This will make the plugin available
in the plugin list of Electron Cash.

### Creating a release

Run `contrib/deterministic_zip` to generate a zip file that can be installed as an external plugin.


### Release process

* Tag a release in gitlab.
* Have multiple individuals check out the tag and generate the zip file.
* Compare hases, they must be the same.
* Have individuals sign the checksum.
* Publish the zip along with checksum and signatures of individuals.


### Tests

To run the tests, you need to have cloned & built the Electron-Cash repo. The tests will look for it at `ELECTRON_CASH_HOME`, or default to `~/Electron-Cash/` if the environment variable is not set.

To run tests, run
```
python3 -m unittest discover
```

### Updating the Qt User Interface

To get the pyuic5 tool, run `pip install pyqt5-tools`. The user interface is
declared in the `ui.ui` file, then converted to Python using the `make_ui.sh`
script.

#### Building Electron Cash

- cd ~
- `git clone https://github.com/Electron-Cash/Electron-Cash.git`
- Build Electron-Cash (see [README.rst](https://github.com/Electron-Cash/Electron-Cash/blob/master/README.rst))
